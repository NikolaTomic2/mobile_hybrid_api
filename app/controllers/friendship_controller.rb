class FriendshipController < ApplicationController
  before_action :authorized
  before_action :find_friend, except: [:get_friends, :get_pending_friends, :get_requested_friends]

  def get_friends
    render json: @user.friends
  end

  def get_pending_friends
    render json: @user.pending_friends
  end

  def get_requested_friends
    render json: @user.requested_friends
  end

  def send_request
    @user.friend_request(@friend)
  end

  def accept_request
    @user.accept_request(@friend)
  end

  def decline_request
    @user.decline_request(@friend)
  end

  def remove_friend
    @user.remove_friend(@friend)
  end

  private

  def find_friend
    begin
        friend = User.find_by(username: params[:username]) || User.find(params[:id])
        @friend = friend
    rescue Exception => error
      render json: {error: "User not found"}, status: :not_found
      return false
    end
  end
end
