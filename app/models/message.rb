class Message < ApplicationRecord
    belongs_to :from, class_name: "User"
    belongs_to :to, class_name: "User"

    validates :data, presence: true

    after_commit { NewMessageJob.perform_later(self) }

    def as_json(options={})
        message = super(options)
        begin
            from_user = User.find(message["from_id"])
            message = message.merge({"from_username" => from_user.username})
        rescue
        end
        message
    end
end
