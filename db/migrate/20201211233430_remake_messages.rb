class RemakeMessages < ActiveRecord::Migration[6.0]
  def change
    drop_table :messages, force: :cascade
    create_table :messages do |t|
      t.belongs_to :to, foreign_key: {to_table: :users}
      t.belongs_to :from, foreign_key: {to_table: :users}

      t.string :data

      t.timestamps
    end
  end
end
