class AddEmailRemoveAge < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :email, :string
    remove_column :users, :age, :integer
  end
end
